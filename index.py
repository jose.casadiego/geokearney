import dash_bootstrap_components as dbc
from dash import dcc
from dash import html
from dash.dependencies import Input, Output, State


from app import app
from apps import home, about, geo

button_bar = dbc.Row(
    [
        dbc.Col(
            dbc.NavItem(dbc.NavLink("Inicio", href="/home")),
            width="auto",
        ),
        dbc.Col(
            dbc.NavItem(dbc.NavLink("Descripción", href="/about")),
            width="auto",
        ),
        dbc.Col(
            dbc.Button(
                "App", color="primary", href="/app", outline=True, className="ms-2"
            ),
            width="auto",
        ),
    ],
    className="g-0 ms-auto flex-nowrap mt-3 mt-md-0",
    align="center",
)

navbar = dbc.Navbar(
    dbc.Container(
        [
            html.A(
                # Use row and col to control vertical alignment of logo / brand
                dbc.Row(
                    [
                        # dbc.Col(html.Img(src="assets/pics/Kearney.png", height="30px")),
                        dbc.Col(
                            dbc.NavbarBrand("Kearney Geoespacial", className="ms-2")
                        ),
                    ],
                    align="center",
                    className="g-0",
                ),
                href="/home",
                style={"textDecoration": "none"},
            ),
            dbc.NavbarToggler(id="navbar-toggler", n_clicks=0),
            dbc.Collapse(
                button_bar,
                id="navbar-collapse",
                is_open=False,
                navbar=True,
            ),
        ]
    ),
    color="light",
    dark=False,
    sticky="top",
)

footer = html.Div(
    html.P("© 2022 ATKearney, All rights reserved", style={"text-align": "center"}),
    style={"padding-top": "25px"},
)

app.layout = html.Div(
    [dcc.Location(id="url", refresh=False), navbar, html.Div(id="page-content"), footer]
)


@app.callback(
    Output("navbar-collapse", "is_open"),
    [Input("navbar-toggler", "n_clicks")],
    [State("navbar-collapse", "is_open")],
)
def toggle_navbar_collapse(n, is_open):
    if n:
        return not is_open
    return is_open


@app.callback(Output("page-content", "children"), Input("url", "pathname"))
def display_page(pathname):
    if pathname == "/home" or pathname == "/":
        return home.layout
    elif pathname == "/about":
        return about.layout
    elif pathname == "/app":
        return geo.layout
    else:
        return "404"


if __name__ == "__main__":
    app.run_server(host="0.0.0.0", port="8000", debug=False)
