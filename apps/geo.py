import dash_bootstrap_components as dbc
import os
import pandas as pd
import base64
from dash import html
from dash.dependencies import Input, Output, State
from .constants import (
    COLUMNS_HOTELS_DB,
    COLUMNS_HOSPITALS_DB,
    COLUMNS_SHOPPING_CENTERS_DB,
)

from app import app, TIMEOUT, cache


def dynamic_select(list_of_elements):
    return dbc.Select(
        options=[
            {"label": district, "value": str(count + 1)}
            for count, district in enumerate(list_of_elements)
        ],
    )


def info_card(name, income, ebit, employees, productivity):

    info_cards = dbc.Card(
        [
            dbc.CardHeader(f"{name}"),
            dbc.CardBody(
                [
                    html.P(f"Ingresos (€k): {income}"),
                    html.P(f"EBIT (€k): {ebit}"),
                    html.P(f"Empleados: {employees}"),
                    html.P(f"Ingresos/Empleados (€k): {productivity}"),
                ]
            ),
        ],
    )

    return info_cards


dict_kpis = {"1": "Ingresos (€k)", "2": "Empleados", "3": "Ing. por Empleados (€k)"}

separator = html.Div(style={"padding-top": "25px"})

select_cluster_1 = dbc.Select(
    id="cluster_type_1",
    value="todas",
    options=[
        {"label": "Todas", "value": "todas"},
        {"label": "Pequeñas", "value": "pequeñas"},
        {"label": "Medianas", "value": "medianas"},
        {"label": "Grandes", "value": "grandes"},
    ],
)

select_cluster_2 = dbc.Select(
    id="cluster_type_2",
    value="grandes",
    options=[
        {"label": "Pequeñas", "value": "pequeñas"},
        {"label": "Medianas", "value": "medianas"},
        {"label": "Grandes", "value": "grandes"},
    ],
)

select_cluster_3 = dbc.Select(
    id="cluster_type_3",
    value="hab_1-75",
    options=[
        {"label": "Hab. 1-75", "value": "hab_1-75"},
        {"label": "Hab. 76-300", "value": "hab_76-300"},
        {"label": "Hab. 300+", "value": "hab_+300"},
    ],
)

select_cluster_5 = dbc.Select(
    id="cluster_type_5",
    value="residencial",
    options=[
        {"label": "Residencial", "value": "residencial"},
        {"label": "Industrial", "value": "industrial"},
        {"label": "Oficinas", "value": "oficinas"},
        {"label": "Público", "value": "publico"},
        {"label": "Retail", "value": "retail"},
    ],
)

select_kpi = dbc.Select(
    id="kpi_type",
    value="1",
    options=[
        {"label": "Ingresos", "value": "1"},
        {"label": "Empleados", "value": "2"},
        {"label": "Ing./Empleados", "value": "3"},
    ],
)

select_B2B = dbc.Select(
    id="b2b_type",
    value="hotels",
    options=[
        {"label": "Hoteles", "value": "hotels"},
        {"label": "Hospitales por tamaño", "value": "hospitals_aggregated"},
        {"label": "Hospitales por compañía", "value": "hospitals_detailed"},
        {
            "label": "Centros Comerciales por tamaño",
            "value": "shopping_centers_aggregated",
        },
        {
            "label": "Centros Comerciales por compañia",
            "value": "shopping_centers_detailed",
        },
    ],
)

cluster_info = dbc.Card(
    dbc.CardBody(
        [
            html.H6("Información:"),
            select_kpi,
            separator,
            dbc.Spinner(html.Div(id="cluster_info")),
        ]
    )
)

# cluster_info_demand = dbc.Card(
#     dbc.CardBody(
#         [
#             html.H6("Información:"),
#             separator,
#             dbc.Spinner(html.Div(id="cluster_info_demand")),
#         ]
#     )
# )

search_districts = dbc.Card(
    dbc.CardBody(
        [
            html.H6("Provincias:"),
            dbc.Spinner(dbc.Select(id="search_panel_districts", value=1)),
        ]
    )
)

search_neighborhoods = dbc.Card(
    dbc.CardBody(
        [
            html.H6("Barrios:"),
            dbc.Spinner(dbc.Select(id="search_panel_neighborhoods", value=1)),
        ]
    )
)

search_alphabetically = dbc.Card(
    dbc.CardBody(
        [
            html.H6("Filtro:"),
            dbc.Spinner(
                dbc.Select(id="search_panel_districts_alphabetically", value=1)
            ),
        ]
    )
)

search_demand = dbc.Card(
    dbc.CardBody(
        [
            html.H6("Empresas:"),
            dbc.Spinner(dbc.Select(id="search_panel_demand", value=1)),
        ]
    )
)

demand_info = html.Div(
    [
        dbc.Button("Información detallada", id="open-xl", n_clicks=0),
        dbc.Modal(
            [
                dbc.ModalHeader(dbc.ModalTitle("Información detallada - TOP 100")),
                dbc.ModalBody(dbc.Spinner(html.Div(id="cluster_info_demand"))),
            ],
            id="modal-xl",
            size="xl",
            is_open=False,
        ),
    ],
    className="text-center",
)

tab_5 = dbc.Row(
    [
        separator,
        dbc.Col(
            [
                dbc.Card(
                    dbc.CardBody(
                        [
                            html.H6("Segmento:"),
                            select_cluster_5,
                        ]
                    )
                ),
                separator,
                search_neighborhoods,
            ],
            md=3,
        ),
        dbc.Col(
            [dbc.Card(html.Div(id="madrid_detailed_map_per_neighborhood"))],
            md=9,
        ),
    ]
)

tab_4 = dbc.Row(
    [
        separator,
        dbc.Col(
            [
                dbc.Card(
                    html.Iframe(
                        srcDoc=open(
                            "assets/maps/demand/b2c/map_b2c_demand.html",
                            "r",
                            encoding="utf-8",
                        ).read(),
                        width="100%",
                        height="702",
                    )
                )
            ],
            md=12,
        ),
    ]
)

tab_3 = dbc.Row(
    [
        separator,
        dbc.Col(
            [
                dbc.Card(
                    dbc.CardBody(
                        [
                            html.H6("Grupo:"),
                            select_B2B,
                        ]
                    )
                ),
                separator,
                search_demand,
                separator,
                demand_info,
            ],
            md=3,
        ),
        dbc.Col(
            [dbc.Card(html.Div(id="spanish_map_demand_b2b"))],
            md=9,
        ),
    ]
)

tab_2 = dbc.Row(
    [
        separator,
        dbc.Col(
            [
                dbc.Card(
                    dbc.CardBody(
                        [
                            html.H6("Empresas:"),
                            select_cluster_2,
                        ]
                    )
                ),
                separator,
                search_districts,
                separator,
                search_alphabetically,
            ],
            md=3,
        ),
        dbc.Col(
            dbc.Spinner(html.Div(id="subset_tech_cards")),
            md=9,
        ),
    ]
)

tab_1 = dbc.Row(
    [
        separator,
        dbc.Col(
            [
                dbc.Card(
                    dbc.CardBody(
                        [
                            html.H6("Empresas:"),
                            select_cluster_1,
                        ]
                    )
                ),
                separator,
                cluster_info,
            ],
            md=3,
        ),
        dbc.Col(
            [dbc.Card(html.Div(id="spanish_map"))],
            md=9,
        ),
    ]
)

layout = dbc.Container(
    [
        dbc.Tabs(
            [
                dbc.Tab(tab_1, label="Oferta"),
                dbc.Tab(tab_2, label="Fichas Técnicas"),
                dbc.Tab(tab_3, label="Demanda B2B/B2G"),
                dbc.Tab(tab_4, label="Oferta - Demanda"),
                dbc.Tab(tab_5, label="Simulador"),
            ]
        )
    ],
    style={"padding-top": "25px"},
)

############################################################################################################################################
########## Callbacks ######################## Callbacks ######################## Callbacks ######################## Callbacks ##############
############################################################################################################################################


@app.callback(
    Output("modal-xl", "is_open"),
    Input("open-xl", "n_clicks"),
    State("modal-xl", "is_open"),
)
def toggle_modal(n1, is_open):
    if n1:
        return not is_open
    return is_open


@app.callback(Output("spanish_map", "children"), [Input("cluster_type_1", "value")])
def display_map(value_cluster):

    url = f"assets/maps/offer/{value_cluster}.html"

    return html.Iframe(
        srcDoc=open(
            url,
            "r",
            encoding="utf-8",
        ).read(),
        width="100%",
        height="702",
    )


@app.callback(
    Output("madrid_detailed_map_per_neighborhood", "children"),
    [
        Input("cluster_type_5", "value"),
        Input("search_panel_neighborhoods", "value"),
        Input("search_panel_neighborhoods", "options"),
    ],
)
def display_map_demand_madrid_per_neighborhood(
    value_cluster_group, value_cluster_element, options_demand
):

    if value_cluster_element == 1:
        value_cluster_element = options_demand[0]["value"]

    url = f"assets/maps/demand/b2c/Madrid/{value_cluster_group}/map_madrid_{value_cluster_element}.html"

    if not os.path.exists(url):
        value_cluster_element = options_demand[0]["value"]
        url = f"assets/maps/demand/b2c/Madrid/{value_cluster_group}/map_madrid_{value_cluster_element}.html"

    return html.Iframe(
        srcDoc=open(
            url,
            "r",
            encoding="utf-8",
        ).read(),
        width="100%",
        height="702",
    )


@app.callback(
    Output("spanish_map_demand_b2b", "children"),
    [
        Input("b2b_type", "value"),
        Input("search_panel_demand", "value"),
        Input("search_panel_demand", "options"),
    ],
)
def display_map_demand(value_cluster_group, value_cluster_element, options_demand):

    if value_cluster_element == 1:
        value_cluster_element = options_demand[0]["value"]

    url = f"assets/maps/demand/{value_cluster_group}/{value_cluster_element}"

    if not os.path.exists(url):
        value_cluster_element = options_demand[0]["value"]
        url = f"assets/maps/demand/{value_cluster_group}/{value_cluster_element}"

    return html.Iframe(
        srcDoc=open(
            url,
            "r",
            encoding="utf-8",
        ).read(),
        width="100%",
        height="702",
    )


@app.callback(
    Output("cluster_info", "children"),
    [Input("cluster_type_1", "value"), Input("kpi_type", "value")],
)
def detailed_information(value_cluster, value_kpi):

    df = query_data()
    if value_cluster == "todas":
        df_describe = df.describe().round(0)
    else:
        df_filtered = df.loc[df[f"cluster_{value_cluster}"] > 0]
        df_describe = df_filtered.describe().round(0)

    df_describe.drop("std", axis=0, inplace=True)
    df_describe = df_describe.reset_index()[["index", dict_kpis[str(value_kpi)]]]
    df_describe.rename(columns={"index": "stat"}, inplace=True)
    df_describe["stat"].replace(
        {"count": "Total Empresas", "min": "Min", "mean": "Media", "max": "Max"},
        inplace=True,
    )

    return dbc.Table.from_dataframe(
        df_describe, striped=True, bordered=True, hover=True, size="sm"
    )


@app.callback(
    Output("cluster_info_demand", "children"),
    [
        Input("b2b_type", "value"),
        Input("search_panel_demand", "value"),
        Input("search_panel_demand", "options"),
    ],
)
def detailed_information_demand(value_b2b, value_cluster, options_cluster):

    if value_b2b == "hotels":
        df = query_data_hotels()
        if value_cluster == 1:
            value_cluster = options_cluster[0]["value"]

        url = f"assets/maps/demand/{value_b2b}/{value_cluster}"

        if not os.path.exists(url):
            value_cluster = options_cluster[0]["value"]

        df_filtered = df.loc[df[value_cluster[:-5]] > 0][COLUMNS_HOTELS_DB]
        df_filtered.sort_values(ascending=False, inplace=True, by="num_habitaciones")
        df_filtered = df_filtered.head(100)

        return dbc.Table.from_dataframe(
            df_filtered, striped=True, bordered=True, hover=True, size="sm"
        )

    elif value_b2b == "hospitals_aggregated":
        df = query_data_hospitals()
        if value_cluster == 1:
            value_cluster = options_cluster[0]["value"]

        url = f"assets/maps/demand/{value_b2b}/{value_cluster}"

        if not os.path.exists(url):
            value_cluster = options_cluster[0]["value"]

        df_filtered = df.loc[df[value_cluster[:-5].replace("_", " ")] > 0][
            COLUMNS_HOSPITALS_DB
        ]
        df_filtered.sort_values(ascending=False, inplace=True, by="CAMAS")
        df_filtered = df_filtered.head(100)

        return dbc.Table.from_dataframe(
            df_filtered, striped=True, bordered=True, hover=True, size="sm"
        )

    elif value_b2b == "shopping_centers_aggregated":
        df = query_data_shopping_centers()
        if value_cluster == 1:
            value_cluster = options_cluster[0]["value"]

        url = f"assets/maps/demand/{value_b2b}/{value_cluster}"

        if not os.path.exists(url):
            value_cluster = options_cluster[0]["value"]

        df_filtered = df.loc[df[value_cluster[:-5].replace("_", " ")] > 0][
            COLUMNS_SHOPPING_CENTERS_DB
        ]
        df_filtered.sort_values(ascending=False, inplace=True, by="superficie_m2")
        df_filtered = df_filtered.head(100)

        return dbc.Table.from_dataframe(
            df_filtered, striped=True, bordered=True, hover=True, size="sm"
        )

    elif value_b2b == "hospitals_detailed":
        df = query_data_hospitals()

        if value_cluster == 1:
            value_cluster = options_cluster[0]["value"]

        url = f"assets/maps/demand/{value_b2b}/{value_cluster}"

        if not os.path.exists(url):
            value_cluster = options_cluster[0]["value"]

        df_filtered = df.loc[df["Grupo"] == value_cluster[:-5].replace("_", " ")][
            COLUMNS_HOSPITALS_DB
        ]
        df_filtered.sort_values(ascending=False, inplace=True, by="CAMAS")
        df_filtered = df_filtered.head(100)
        df_filtered.append(df_filtered.sum(numeric_only=True), ignore_index=True)

        return html.Div(
            [
                dbc.Table.from_dataframe(
                    df_filtered.append(
                        df_filtered.sum(numeric_only=True), ignore_index=True
                    ),
                    striped=True,
                    bordered=True,
                    hover=True,
                    size="sm",
                ),
            ]
        )

    elif value_b2b == "shopping_centers_detailed":
        df = query_data_shopping_centers()
        if value_cluster == 1:
            value_cluster = options_cluster[0]["value"]

        url = f"assets/maps/demand/{value_b2b}/{value_cluster}"

        if not os.path.exists(url):
            value_cluster = options_cluster[0]["value"]

        df_filtered = df.loc[df["grupo"] == value_cluster[:-5].replace("_", " ")][
            COLUMNS_SHOPPING_CENTERS_DB
        ]
        df_filtered.sort_values(ascending=False, inplace=True, by="superficie_m2")
        df_filtered = df_filtered.head(100)
        df_filtered.append(df.sum(numeric_only=True), ignore_index=True)

        return html.Div(
            [
                dbc.Table.from_dataframe(
                    df_filtered.append(
                        df_filtered.sum(numeric_only=True), ignore_index=True
                    ),
                    striped=True,
                    bordered=True,
                    hover=True,
                    size="sm",
                ),
            ]
        )

    else:
        pass


@app.callback(
    Output("subset_tech_cards", "children"),
    [
        Input("cluster_type_2", "value"),
        Input("search_panel_districts", "value"),
        Input("search_panel_districts", "options"),
        Input("search_panel_districts_alphabetically", "value"),
        Input("search_panel_districts_alphabetically", "options"),
    ],
)
def tech_cards(
    value_cluster, value_district, options_district, value_alphabet, options_alphabet
):

    df = query_data()

    if value_district == 1:
        value_district = options_district[0]["label"]

    list_options = [option["value"] for option in options_alphabet]
    if (value_alphabet == 1) or (value_alphabet not in list_options):
        value_alphabet = options_alphabet[0]["label"]

    df.sort_values(by="nombre", inplace=True)

    list_fichas = df.loc[
        (df["provincia_hq"] == value_district)
        & (df[f"cluster_{value_cluster}"] > 0)
        & (df["nombre"].str.startswith(value_alphabet)),
        "Ficha",
    ].tolist()

    return html.Div([eval(ficha) for ficha in list_fichas])


@app.callback(
    Output("search_panel_neighborhoods", "options"),
    [Input("cluster_type_5", "value")],
)
def neighborhood_panel(value_cluster):
    df = query_data_catastro_madrid()

    df = df.loc[~df["barrio"].isna()]
    df.sort_values(by="barrio", inplace=True)
    list_of_neighborhoods = df["barrio"].unique().tolist()

    return [
        {"label": neighborhood, "value": neighborhood}
        for neighborhood in list_of_neighborhoods
    ]


@app.callback(
    Output("search_panel_districts", "options"),
    [Input("cluster_type_2", "value")],
)
def district_panel(value_cluster):
    df = query_data()
    if value_cluster != "todas":
        df = df.loc[df[f"cluster_{value_cluster}"] > 0]

    df = df.loc[~df["provincia_hq"].isna()]
    df.sort_values(by="provincia_hq", inplace=True)
    list_of_districts = df["provincia_hq"].unique().tolist()

    return [{"label": district, "value": district} for district in list_of_districts]


@app.callback(
    Output("search_panel_districts_alphabetically", "options"),
    [
        Input("cluster_type_2", "value"),
        Input("search_panel_districts", "value"),
        Input("search_panel_districts", "options"),
    ],
)
def district_panel_alphabetically(value_cluster, value_district, options_district):

    df = query_data()
    if value_cluster != "todas":
        df = df.loc[df[f"cluster_{value_cluster}"] > 0]

    if value_district == 1:
        value_district = options_district[0]["label"]

    df = df.loc[df["provincia_hq"] == value_district]
    df.sort_values(by="nombre", inplace=True)
    list_of_filters = df["nombre"].str[0].unique().tolist()

    return [{"label": filter, "value": filter} for filter in list_of_filters]


@app.callback(
    Output("search_panel_demand", "options"),
    [Input("b2b_type", "value")],
)
def demand_panel(value_type):

    list_options = os.listdir(f"assets/maps/demand/{value_type}")

    return [
        {"label": option[:-5].replace("_", " "), "value": option}
        for option in list_options
    ]


@cache.memoize(timeout=TIMEOUT)
def query_data():
    df = pd.read_excel("assets/db/DB.xlsx")
    return df


@cache.memoize(timeout=TIMEOUT)
def query_data_hospitals():
    df = pd.read_excel("assets/db/CNH_2021_clean.xlsx")
    return df


@cache.memoize(timeout=TIMEOUT)
def query_data_shopping_centers():
    df = pd.read_excel("assets/db/Centros_comerciales_clean_v02.xlsx")
    return df


@cache.memoize(timeout=TIMEOUT)
def query_data_hotels():
    df = pd.read_excel("assets/db/BBDD_hoteles_limpia.xlsx")
    return df


@cache.memoize(timeout=TIMEOUT)
def query_data_catastro_madrid():
    df = pd.read_csv("assets/db/DB_Madrid_Catastro.csv")
    return df
