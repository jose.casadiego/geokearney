import dash_bootstrap_components as dbc
from dash import dcc
from dash import html
from dash.dependencies import Input, Output

from app import app

layout = dbc.Container(
    [
        dbc.Row(
            [
                dbc.Col(
                    dbc.Card(
                        [
                            dbc.CardImg(src="/assets/pics/card1.jpg", top=True),
                            dbc.CardHeader(
                                "ANÁLISIS GEOESPACIAL DEL MERCADO",
                                className="h6 text-center",
                            ),
                            dbc.CardBody(
                                [
                                    html.Div(
                                        html.P(
                                            "Visión geoespacial de la oferta de empresas de servicios de rehabilitación y renovación de España a nivel autonómico, provincial y local",
                                            className="card-text text-center",
                                        ),
                                        style={"height": "100px"},
                                    )
                                ],
                            ),
                        ],
                    ),
                    md=4,
                ),
                dbc.Col(
                    dbc.Card(
                        [
                            dbc.CardImg(src="/assets/pics/card2.jpg", top=True),
                            dbc.CardHeader(
                                "ANÁLISIS INDIVIDUAL POR EMPRESA \n (Work in progress)",
                                className="h6 text-center",
                            ),
                            dbc.CardBody(
                                [
                                    html.Div(
                                        html.P(
                                            "Visión detallada de cada empresa del mercado carácterizando su capacidad de ejecución y radio de acción",
                                            className="card-text text-center",
                                        ),
                                        style={"height": "100px"},
                                    ),
                                ],
                            ),
                        ],
                    ),
                    md=4,
                ),
                dbc.Col(
                    dbc.Card(
                        [
                            dbc.CardImg(src="/assets/pics/card3.jpg", top=True),
                            dbc.CardHeader(
                                "OPTIMIZACIÓN OFERTA-DEMANDA \n (Work in progress)",
                                className="h6 text-center",
                            ),
                            dbc.CardBody(
                                html.Div(
                                    html.P(
                                        "Definición del modelo óptimo de oferta para atender la demanda en los segmentos y geografías prioritarias",
                                        className="card-text text-center",
                                    ),
                                    style={"height": "100px"},
                                )
                            ),
                        ],
                    ),
                    md=4,
                ),
            ]
        ),
    ],
    style={"padding-top": "25px"},
)
