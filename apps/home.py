import dash_bootstrap_components as dbc
from dash import dcc
from dash import html
from dash.dependencies import Input, Output
import dash_gif_component as gif

from app import app

layout = dbc.Container(
    dbc.Row(
        [
            dbc.Col(
                html.Div(
                    [
                        html.H2(
                            "PLATAFORMA DE INTELIGENCIA GEOESPACIAL",
                            className="display-5 text-white",
                            style={"padding-top": "30px"},
                        ),
                        # html.Hr(className="my-2"),
                        # html.P(
                        #     "Herramienta de análisis geoespacial de la oferta de constructoras de España, "
                        #     "su alcance y capacidad de absorción de demanda",
                        #     className="lead text-white",
                        # ),
                    ],
                    className="h-100 p-5 text-white bg-dark rounded-3 align-middle",
                ),
                md=6,
            ),
            dbc.Col(
                html.Div(
                    [
                        gif.GifPlayer(
                            gif="assets/gifs/KG_example.gif",
                            still="assets/pics/gif_still.jpg",
                            autoplay=True,
                        )
                    ],
                    className="h-100 p-5 bg-light border rounded-3",
                ),
                md=6,
            ),
        ],
    ),
    style={"padding-top": "25px"},
)


@app.callback(
    Output("app-1-display-value", "children"), Input("app-1-dropdown", "value")
)
def display_value(value):
    return 'You have selected "{}"'.format(value)
